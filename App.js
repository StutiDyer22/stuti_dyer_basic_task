import React, { Component } from 'react'
import { View } from 'react-native'
import MaNavigator from "./src/components/Navigation/Drawer"
import {Provider} from 'react-redux';
import {applyMiddleware,createStore} from "redux";
import reducers from "./src/Reducres";
import ReduxThunk from 'redux-thunk';
// import AsyncStorage from '@react-native-community/async-storage';

export default class App extends Component {
  render() {
    const store = createStore(reducers,{},applyMiddleware(ReduxThunk));
    return (
     <Provider store={store}>
          <View style={{flex:1}}>
         <MaNavigator/>
         </View>
     </Provider>
       
     
    )
  }
}


