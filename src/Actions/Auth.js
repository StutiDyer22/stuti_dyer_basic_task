import { Alert } from 'react-native';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  NAME_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  REGISTER_USER,
  REGISTER_USER_FAIL,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_PROFILE,
  FIRSTNAME_CHANGED,
  LASTNAME_CHANGED,
  PHONE_CHANGED,
  JOIN_DATE_CHANGED,
  USER_PROFILE_UPDATE,
  USER_PROFILE_UPDATE_SUCCESS,
  USER_PROFILE_UPDATE_FAIL,
  ADDRESS_CHANGED,
  SHOW_AUTH_MODAL,
  HIDE_AUTH_MODAL,
} from "./Type";


export const showAuthModal = () => dispatch => dispatch({ type: SHOW_AUTH_MODAL });

export const hideAuthModal = () => dispatch => dispatch({ type: HIDE_AUTH_MODAL });

export const emailChanged = text => ({
  type: EMAIL_CHANGED,
  payload: text,
});

export const passwordChanged = text => ({
  type: PASSWORD_CHANGED,
  payload: text,
});

export const nameChanged = text => ({
  type: NAME_CHANGED,
  payload: text,
});

export const fisrtNameChanged = text => ({
  type:FIRSTNAME_CHANGED,
  payload:text
});

export const lastNameChanged = text => ({
  type:LASTNAME_CHANGED,
  payload:text
});
export const addressChanged = text => ({
  type:ADDRESS_CHANGED,
  payload:text
});
export const phoneChanged = text => ({
  type:PHONE_CHANGED,
  payload:text
});
export const joinDateChanged = text => ({
  type:JOIN_DATE_CHANGED,
  payload:text
});

export const loginUser = (username, password, navigation) => {
  console.log('STAE', username, password);
  return (dispatch,getState) => {
    const state = getState().auth;
    console.log("state",state);
    dispatch({ type: LOGIN_USER });
    try{
      if(state.username === username && state.password === password){
        navigation.navigate('Home');
        dispatch({
          type: LOGIN_USER_SUCCESS,
          payload: username,
        });
        console.log("success",username,password);
      }else{
        console.log('ERROR');
        dispatch({ type: LOGIN_USER_FAIL });
      }  
    }
    catch{error => {
        console.log('ERROE', error);
        dispatch({ type: LOGIN_USER_FAIL });
      };
    }
  };
};

export const registerUser = (
  username,
  email,
  password,
  navigation
) => (dispatch,getState) => {
  try{
    dispatch({ type: REGISTER_USER });
  const state = getState();
  // if(username,email,password){
    dispatch({type: REGISTER_USER_SUCCESS});
    state.username = username;
    state.email = email;
    state.password = password;
    console.log("state register",state)
    dispatch({type: REGISTER_USER_PROFILE});
    navigation.navigate('Home');
  }catch(e){
    console.log("register err")
     dispatch({ type: REGISTER_USER_FAIL });
  }

};
export const editUser = (
  firstName,lastName,address,phone,email,join_date
) => (dispatch,getState) => {
  const state = getState();
  try{
    dispatch({type:USER_PROFILE_UPDATE});
  state.firstName = firstName;
  state.lastName = lastName;
  state.address = address;
  state.phone = phone;
  state.email = email;
  state.join_date = join_date;
  dispatch({USER_PROFILE_UPDATE_SUCCESS})
}catch(error){
    console.log('update profile fail')
    dispatch({type:USER_PROFILE_UPDATE_FAIL})
  }

}


