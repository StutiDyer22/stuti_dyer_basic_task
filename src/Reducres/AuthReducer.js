import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    NAME_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,
    // USER_PROFILE,
    REGISTER_USER,
    REGISTER_USER_FAIL,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_PROFILE,
    FIRSTNAME_CHANGED,
    LASTNAME_CHANGED,
    PHONE_CHANGED,
    ADDRESS_CHANGED,
    JOIN_DATE_CHANGED,
    SHOW_AUTH_MODAL,
    USER_PROFILE_UPDATE,
    USER_PROFILE_UPDATE_SUCCESS,
    HIDE_AUTH_MODAL,
    USER_PROFILE_UPDATE_FAIL,
} from "../Actions/Type"


const INITIAL_STATE = {
    firstName:"",
    lastName:'',
    phone:'',
    address:'',
    join_date:'',
    email: "",
    password: "",
    username: "",
    error: null, 
    loading: false,
    userData: null,
    success: false,
    fail: false,
    isModalVisible: true
}
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case EMAIL_CHANGED:
            console.log("email changed", action.payload)
            return {
                ...state, email: action.payload, fail: false
            };
        case PASSWORD_CHANGED:
            console.log("password changed", action.payload)
            return {
                ...state, password: action.payload, fail: false
            };
        case NAME_CHANGED:
            console.log("username changed", action.payload)
            return {
                ...state, username: action.payload
            };
        case LOGIN_USER:
            return {
                ...state, loading: true, error: ''
            };
        case REGISTER_USER:
            return {
                ...state, loading: true, error: ''
            };
        case LOGIN_USER_SUCCESS:
            console.log("login user success", action.payload);
            return {
                ...state,
                ...INITIAL_STATE,
                user: action.payload,
                    error: null,
                    loading: false,
            };
        case FIRSTNAME_CHANGED:
        console.log("firstname changed", action.payload)
        return{
            ...state,firstName:action.payload
        };
        case LASTNAME_CHANGED:
        console.log("lastname", action.payload)
        return{
            ...state,lastName:action.payload
        };
        case ADDRESS_CHANGED:
        console.log("address", action.payload)
        return{
            ...state,address:action.payload
        };
        case PHONE_CHANGED:
        console.log("phone", action.payload)
        return{
            ...state,phone:action.payload
        };
        case JOIN_DATE_CHANGED:
        console.log("join date", action.payload)
        return{
            ...state,join_date:action.payload
        };
        case REGISTER_USER_SUCCESS:
            console.log("register user success");
            return {
                ...state,
                ...INITIAL_STATE,
                error: null,
                    isModalVisible: true,
                    success: true,
                    fail: false,
            };
            case USER_PROFILE_UPDATE_SUCCESS:
            console.log("user update success");
            return{
                ...state,
                ...INITIAL_STATE,
                error:null,
                isModalVisible:true,
                success:true,
                fail:false
            };
            case USER_PROFILE_UPDATE:
            console.log("user profile update");
            return {
                ...state,error:null,loading:false
            };
        case REGISTER_USER_PROFILE:
            console.log("register user profile");
            return {
                ...state, error: null, loading: false
            };
        case LOGIN_USER_FAIL:
            console.log("login user fail");
            return {
                ...state, fail: true, success: false, error: "Authentication Failed", loading: false, isModalVisible: true
            };
        case REGISTER_USER_FAIL:
            return {
                ...state,
                password: '',
                    fail: true,
                    isModalVisible: true,
                    error: "Authentication Failed.",
                    loading: false,
                    success: false
            };
        case USER_PROFILE_UPDATE_FAIL:
        return{
            ...state, fail: true, success: false, error: "Update fail", loading: false, isModalVisible: true
            
        };
        case SHOW_AUTH_MODAL:
            return {
                ...state, isModalVisible: true, fail: false
            };
        case HIDE_AUTH_MODAL:
            return {
                ...state, isModalVisible: false, fail: false
            };
        default:
            return state;
    }
}