import React, { Component } from 'react'
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  TouchableHighlight,
  TextInput
} from 'react-native'
import { Icon } from 'react-native-elements'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'
var { width, height } = Dimensions.get('window')

let randomHex = () => {
  
  // let letters = '0123456789ABCDEF';
  let color = 'red';
  // for (let i = 0; i < 6; i++ ) {
  //     color += letters[Math.floor(Math.random() * 16)];
  // }
  return color;
}
export default class TestStrip extends Component {
  constructor(props){
    super(props)
    // this.onClick = this.onClick.bind(this);
    this.state = {
      // borderColor: randomHex(),
      row1:'0',
      row2:'0',
      row3:'0',
      row4:'0',
      row5:'0',
      status:false
    }
  }
  
  onClick = (row,value) =>{
    this.setState({status:"true"});
    this.setState({[row]:value});
    
    // this.setState({[borderColor]:'yellow'}); 
} 
  render () {
    return (
      <View style={{ flex: 1, margin: '5%' }}>
      {this.state.status?
        <View style={{ flexDirection: 'row' }}>
          <Icon
            name={'arrow-left'}
            type='font-awesome'
            color='#8a9199'
            size={25}
            onPress={() => this.props.navigation.navigate('Home')}
            containerStyle={{ justifyContent: 'flex-start', marginLeft: '2%' }}
          />
          <View
            style={{
              flex: 8,
              justifyContent: 'flex-end',
              flexDirection: 'row',
              marginRight: 10
            }}
          >
            <TouchableOpacity style={styles.buttonStyle}>
              <Text
                style={{
                  color: 'white',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                Next
              </Text>
            </TouchableOpacity>
          </View>
        </View>:null}
        <Text style={styles.headerStyle}> Test Strip </Text>
        <View style={{ flex: 0.1  }}>
          <View style={styles.leftDividerStyle}>
            <View style={styles.horizontalTopLineStyle}>
              <View style={styles.rightDividerStyle}>
                <View
                  style={{ height: 20, marginTop: 70, backgroundColor: 'red' }}
                />
                <View
                  style={{ height: 20, marginTop: 111, backgroundColor: 'red' }}
                />
                <View
                  style={{ height: 20, marginTop: 108, backgroundColor: 'red' }}
                />
                <View
                  style={{ height: 20, marginTop: 105, backgroundColor: 'red' }}
                />
                <View
                  style={{ height: 20, marginTop: 110, backgroundColor: 'red' }}
                />
              </View>
            </View>
          </View>
          <View style={styles.horizontalBottomLineStyle} />
        </View>

          <View style={{ flex: 1}}>
          <View style={{ flex: 1,left:25}}>
            <View
              style={styles.header1Style}
            >
              <Text style={styles.titleStyle}>
                Total Hardness <Text style={styles.subtitleStyle}> (ppm)</Text>
              </Text>
              {/* <View style={{margin:'4%'}}></View> */}
              
              <TextInput
                mode='outlined'
                onChangeText={(text) => this.setState({row1:text})}
                placeholder="0"
                value={this.state.row1}
                style={styles.inputStyle}
              />
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent:  "space-evenly"}}
            >
              <View>
              <TouchableHighlight  onPress={() => this.onClick('row1','0')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>0</Text>
              </View>
              <View>
              <TouchableHighlight  onPress={() => this.onClick('row1','110')}>
              <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>110</Text>
              </View>
              <View>
              <TouchableHighlight  onPress={() => this.onClick('row1','250')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>250</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row1','500')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>500</Text>
                
              </View>
              
              <View>
              <TouchableHighlight  onPress={() => this.onClick('row1','1000')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>1000</Text>
              </View>
            </View>
          </View>
          </View>
          <View style={{ flex: 1,left:20 }}>
            <View
              style={styles.header1Style}
            >
              <Text style={styles.titleStyle}>
                Total Hardness <Text style={styles.subtitleStyle}> (ppm)</Text>
              </Text>
              <TextInput
                mode='outlined'
                placeholder='0'
                onChangeText={(text) => this.setState({row2:text})}
                value={this.state.row2}
                style={styles.inputStyle}
              />
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}
            >
              <View>
              <TouchableHighlight onPress={() => this.onClick('row2','0')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>0</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row2','110')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>110</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row2','250')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>250</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row2','500')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>500</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row2','1000')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>1000</Text>
                
              </View>
            </View>
          </View>
          <View style={{ flex: 1 ,left:20}}>
            <View
              style={styles.header1Style}
            >
              <Text style={styles.titleStyle}>
                Total Hardness <Text style={styles.subtitleStyle}> (ppm)</Text>
              </Text>
              <TextInput
                mode='outlined'
                placeholder='0'
                onChangeText={(text) => this.setState({row3:text})}    
                value={this.state.row3}
                style={styles.inputStyle}
              />
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}
            >
              <View>
              <TouchableHighlight onPress={() => this.onClick('row3','0')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>0</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row3','110')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>110</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row3','250')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>250</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row3','500')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>500</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row3','1000')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>1000</Text>
                
              </View>
            </View>
          </View>
          <View style={{ flex: 1 ,left:20}}>
            <View
              style={styles.header1Style}
            >
              <Text style={styles.titleStyle}>
                Total Hardness <Text style={styles.subtitleStyle}> (ppm)</Text>
              </Text>
              <TextInput
                mode='outlined'
                placeholder='0'
                onChangeText={(text) => this.setState({row4:text})}
                value={this.state.row4}
                style={styles.inputStyle}
              />
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}
            >
              <View>
              <TouchableHighlight onPress={() => this.onClick('row4','0')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>0</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row4','110')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>110</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row4','250')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>250</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row4','500')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>500</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row4','1000')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>1000</Text>
                
              </View>
            </View>
          </View>
          <View style={{ flex: 1 ,left:20}}>
            <View
              style={styles.header1Style}
            >
              <Text style={styles.titleStyle}>
                Total Hardness <Text style={styles.subtitleStyle}> (ppm)</Text>
              </Text>
              <TextInput
                mode='outlined'
                placeholder='0'
                value={this.state.row5}
                onChangeText={(text) => this.setState({row5:text})}
                style={styles.inputStyle}
              />
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}
            >
              <View>
              <TouchableHighlight onPress={()=>{this.setState({row5:'0'})}}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>0</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row5','110')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>110</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row5','250')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>250</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row5','500')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>500</Text>
                
              </View>
              <View>
              <TouchableHighlight onPress={() => this.onClick('row5','1000')}>
                <View style={styles.stripStyle} />
                </TouchableHighlight>
                <Text style={{ textAlign: 'center' }}>1000</Text>
                
              </View>
          
          </View>
        </View>
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header1Style:{
    flexDirection: 'row',
    paddingBottom:20,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  stripStyle: {
    height: hp('2.7%'),
    width: wp('12'),
    borderRadius: 6,
    // borderColor: 'black',
    // borderWidth:1,
    backgroundColor: 'pink'
  },
  inputStyle: {
    backgroundColor: 'white',
    borderWidth: 0.5,
    borderColor: 'grey',
    height: hp('4%'),
    width: wp('17%'),
    borderRadius: 7,
    textAlign: 'center',
    // alignSelf:"center",
    marginTop: 6,
    marginRight:'auto',
    marginLeft:'auto',
    padding: 0
  },
  titleStyle: {
    marginLeft:'auto',
    // paddingBottom:30,
    marginRight:'auto',
    fontWeight: 'bold',
    color: '#afb4ba',
    fontSize: 17
  },
  subtitleStyle: {
    color: '#c4c7cc',
    fontSize: 15
  },
  horizontalTopLineStyle: {
    borderTopWidth: 1,
    borderTopColor: 'black',
    width: 19
  },
  horizontalBottomLineStyle: {
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    width: 19,
    // left: 0
  },
  leftDividerStyle: {
    // flex:1,
    borderLeftWidth: 1,
    borderLeftColor: 'black',
    // marginLeft: 15,
    flexDirection: 'column',
    height: height - 145
  },
  rightDividerStyle: {
    // flex:1,
    borderRightWidth: 1,
    borderRightColor: 'black',
    marginLeft: 1,
    flexDirection: 'column',
    height: height - 145
  },
  buttonStyle: {
    backgroundColor: 'black',
    justifyContent: 'center',
    borderRadius: 30,
    width: '20%',
    height: 26
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: 25,
    // flex: 1,
    margin: 12,
    color: '#0b267d'
  }
})
