import React, { Component } from 'react'
import { Text, StyleSheet, View,ImageBackground,ScrollView,StatusBar } from 'react-native'
import {Icon} from "react-native-elements"
export default class CategoryDetail extends Component {
    render() {
        const {navigation} = this.props;
        console.log("nav",navigation)
        // const CategoryName = 
        return (
            <View style={{flex:1}}>
            <StatusBar backgroundColor="black"></StatusBar>
                <View style={{backgroundColor:"black",flex:1,flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                   <View style={{flex:0.3,paddingLeft:20}}>
                        <Icon name={"angle-left"}  type="font-awesome" color='white' size={30}  onPress={()=>{this.props.navigation.goBack()}}></Icon>
                    </View>
                    <View style={{alignItems:"center",flex:11,paddingBottom:10}}>
                        <Text style={{fontWeight:"bold",color:"white",fontSize:18}}>{navigation.state.params.Category}</Text>
                    </View>
                </View>
              
                <View style={{flex:11}}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <ImageBackground source={require("../../assets/images/beach.jpeg")} style={styles.imageStyle}>
                        <View style={{alignItems:"center",justifyContent:"center"}}>
                            <Text style={styles.textStyle}>{navigation.state.params.PlaceName[0]}</Text>
                            <Text style={styles.subtitleStyle}>Activites</Text>
                        </View>
                    </ImageBackground>
                <ImageBackground source={require("../../assets/images/hotel.jpeg")} style={styles.imageStyle}>
                    <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text style={styles.textStyle}>{navigation.state.params.PlaceName[1]}</Text>
                        <Text style={styles.subtitleStyle}>Restaurants</Text>
                    </View> 
                </ImageBackground>
                <ImageBackground source={require("../../assets/images/hotel2.jpeg")} style={styles.imageStyle}>
                   <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text style={styles.textStyle}>{navigation.state.params.PlaceName[2]}</Text>
                        <Text  style={styles.subtitleStyle}>Hotel</Text>
                    </View>
                </ImageBackground>
                </ScrollView>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    subtitleStyle:{
        color:"white"
    },
    textStyle:{
        color:"white",
        fontWeight:"bold",
        fontSize:20,
        paddingTop:60
        
    },
    imageStyle:{
        padding:0,
        margin:0,
        // paddingVertical: 30,
        width: "100%",
        height: 252,
    },
})
