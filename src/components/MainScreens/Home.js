import React, { Component } from 'react'
import { Text, StyleSheet, View,StatusBar,ScrollView,Image,TouchableOpacity} from 'react-native'
// import DrawerNavigator from "../Navigation/Drawer"
import { Icon } from 'react-native-elements'

export default class Home extends Component {
    render() {
        return (
            <View style={styles.containerStyle}>
                <StatusBar backgroundColor="#E5E5E5"/>
                <View style={{flex:1,flexDirection:"row"}}>
                   <View style={{justifyContent:"flex-start",flex:2}}>
                        <Icon name={"align-left"} type="font-awesome" color='#8a9199' size={25} onPress={()=>this.props.navigation.toggleDrawer()}></Icon>
                    </View>
                    <View style={{flexDirection:"row",flex:10,justifyContent:"center",paddingRight:'15%'}}> 
                        <Text style={{fontSize:17,color:"#171de3"}}>OUR</Text>
                        <Text style={{fontWeight:"bold",fontSize:17,color:"#171de3"}}>CAMPUS</Text>
                    </View>
                </View>                
                    <View style={{top:'3%',marginLeft:'7%',flex:1,flexDirection:"row"}}>
                        <Text style={{fontWeight:"bold",fontSize:25}}>Places</Text>
                    </View>
                    <View style={{paddingLeft:'11%',flex:0.5,flexDirection:"row"}}>
                        <Text style={{fontWeight:"bold",fontSize:18}}>Popular</Text>
                    </View>
                    
                    <View style={styles.mainScroll}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={styles.whiteView}>            
                            <Image source={{uri:'https://picsum.photos/200'}} style={styles.imageStyle}></Image>
                            <Text style={{fontSize:18,paddingLeft:5}}>Snack Valet Park</Text> 
                        </View>
                   
                        <View style={styles.whiteView}>            
                            <Image source={{uri:'https://picsum.photos/200'}} style={styles.imageStyle}></Image>
                            <Text style={{fontSize:18,paddingLeft:5}}>Snack Valet Park</Text> 
                         </View>
                         </ScrollView>
                    </View>
                    <Text style={styles.textStyle}>All Categories</Text>
                    <View style={{alignItems:"center",justifyContent:"center",flex:5}}>
                        <View style={styles.expandBar}>
                            <Text style={styles.expandText}>Beaches</Text>
                            <Icon name={'chevron-right'} type="font-awesome" color='white' onPress={()=>{this.props.navigation.navigate('CategoryDetails',{Category:'Beaches',PlaceName:['Baga beach','Radhanagar Beach','Colva Beach']})}} size={25} containerStyle={{ alignSelf: 'flex-end',transform: [{ translateY: -16 }, { translateX: -12 }]}}></Icon>                        
                        </View>
                        <View style={styles.expandBar}>
                        <Text style={styles.expandText}>Cities</Text>
                        <Icon name={"chevron-right"} type="font-awesome" color='white' onPress={()=>{this.props.navigation.navigate('CategoryDetails',{Category:'Cities',PlaceName:['Ahmedabad','Mumbai','Surat']})}} containerStyle={{ alignSelf: 'flex-end',transform: [{ translateY: -16 }, { translateX: -12 }]}}></Icon>
                        </View>
                        <View style={styles.expandBar}>
                        <Text style={styles.expandText}>Gardens</Text>
                        <Icon name={"chevron-right"}  type="font-awesome" color='white' onPress={()=>{this.props.navigation.navigate('CategoryDetails',{Category:'Gardens',PlaceName:['Auroville Botanical','Brindhavan','Chambal']})}} containerStyle={{ alignSelf: 'flex-end',transform: [{ translateY: -16 }, { translateX: -12 }]}}></Icon>
                        </View>
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    expandText:{
        fontSize:17,
        color:"white",
        paddingLeft:20,
        paddingTop:20,
        // fontWeight:"bold"
    },
    whiteView:{
        marginLeft:10,
        // flex:1,
        // height:'50%',
        width:220,
        // marginLeft:'2%',
        backgroundColor:"white",
        borderRadius:20
    },
    mainScroll:{
        // top:-40,
        flex:3,
        paddingBottom:20,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
        marginLeft:50
    },
    imageStyle:{
        width: '90%',
        height:'80%',
        borderRadius:20,
        transform:[{translateY:-10}],
        alignSelf:"center"
},
    textStyle:{
        flexDirection:"row",    
        flex:0.5,
        top:10,
        fontSize:18,
        fontWeight:"bold",
        marginLeft:60,
        // marginBottom:20
    },
    expandBar:{
        borderRadius:10,
        height:70,
        width:"80%",
        backgroundColor:"#1a6aeb",
        marginBottom:'2%'
    },
    containerStyle:{
        flex:1,
        backgroundColor:"#E5E5E5",
        
    }
})

