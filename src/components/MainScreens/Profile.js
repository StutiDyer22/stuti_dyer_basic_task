import React, { Component } from 'react'
import { Text, StyleSheet, View,TextInput,Image,TouchableOpacity} from 'react-native'
import {TextInputLayout} from 'rn-textinputlayout';
import Modal from 'react-native-modal';
import {Icon} from "react-native-elements";
import {connect} from 'react-redux';
import {
editUser,
fisrtNameChanged,
lastNameChanged,
emailChanged,
addressChanged,
phoneChanged,
joinDateChanged,
showAuthModal,
hideAuthModal} from "../../Actions"
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

 class Profile extends Component {
    constructor(props){
        super(props)
        this.state = {
            edit:false,
            firstNameErr:false,
            lastNameErr:false,
            phoneErr:false,
            addressErr:false,
            joinDataErr:false
          }
    }
    renderError(){
        if(this.props.fail){
            return(
                <View>
                    <Modal isVisible = {this.props.isModalVisible} 
                    style={styles.modalStyle}>
                    <View style={[styles.cardStyle, {height:'30%'}]}>
                    <Image source={require('../../assets/images/drawer.png')} resizeMode='contain' style={{
                    height:"25%", alignSelf:'center'}}/>
                    <Text style={{textAlign:'center', fontFamily:'Comfortaa-Bold', color:'rgb(51,51,51)', margin:'5%'}}>Invalid Credentials!</Text>
                    <TouchableOpacity style={[styles.buttonModal, {height:'30%'}]} onPress={() => this.toggleModal()}>
                        <Text style={{color:'white', textAlign:'center', fontSize:15, fontFamily:'Comfortaa-Bold'}}>Retry</Text>
                    </TouchableOpacity>
                    </View>
                    </Modal>
                </View>
            );
        }
        return null;
    };
    save = () =>{
        console.log("save",this.props)
        const {firstName,lastName,phone,address,join_date,email} = this.props;
        if(firstName.length>1){
            this.setState({firstNameErr:false})
        if(lastName.length>1){
            this.setState({lastNameErr:false})
        if(phone.length<10){
            this.setState({phoneErr:false})
            console.log("updated")
            this.props.editUser(firstName,lastName,phone,address,join_date,email);
        }else{
            console.log("phone err")
            this.setState({phoneErr:true})
        }
    }else{
        console.log("last err")
        this.setState({lastNameErr:true})
        }
    }else{
        console.log("first err")
        this.setState({firstNameErr:true})
        }
    };
        onClick = () =>{
            this.setState({edit:true})
        }

        onFirtNameChanged= (text) =>{
            this.props.fisrtNameChanged(text)
        }
        onLastNameChanged = (text) =>{
            this.props.lastNameChanged(text)
        }
        onAddressChanged = (text) =>{
            this.props.addressChanged(text)
        }
        onEmailChanged = (text) =>{
            this.props.emailChanged(text)
        }
        onNumberChanged = (text) =>{
            this.props.phoneChanged(text)
        }
        onJoinDateChanged = (text) =>{
            this.props.joinDateChanged(text)
        }
      
    render() {
        
        return (
            <View style={{backgroundColor:"#E5E5E5",flex:1}}>
                <View style={{flex:1,flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                   <View style={{justifyContent:"flex-start",flex:1}}>
                        <Icon name={"angle-left"} type="font-awesome" color='#378bde' size={30} onPress={()=>{this.props.navigation.goBack()}}></Icon>
                    </View>
                    <View style={{flex:5,paddingLeft:'10%'}}>
                        <Text style={{textAlign:"center",fontWeight:"bold",color:"#378bde",fontSize:18}}>Profile</Text>
                    </View>
                    <View style={{flex:2}}>
                        <Icon name={"create"} type="material" color='#378bde' onPress={() => this.onClick()}   size={30}></Icon>
                    </View>
                </View>
                 <View style={{flexDirection:"row",flex:2}}>
                    <Image source={{uri:'https://picsum.photos/200'}} style={styles.imageStyle}></Image>
                    <Text style={{fontSize:20,fontWeight:"bold",alignSelf:"center",flex:4}}>Hello,</Text>
                <View style={{flex:1}}/>
             </View>
            <View style={{backgroundColor:"white",margin:20,flex:5,maxHeight:340}}>
            <View style={{flexDirection:"row"}}>

            <TextInputLayout style={styles.inputLayout} focusColor="#d4d4d4">
                    <TextInput
                    value={this.props.firstName}
                    onChangeText = {(text)=>{this.onFirtNameChanged(text)}}
                    editable={this.state.edit ? true : false}
                    style={styles.textInput}
                    placeholder={'First Name'}
                    />
                </TextInputLayout>
                <TextInputLayout style={styles.inputLayout} focusColor="#d4d4d4">
                    <TextInput
                    value={this.props.lastName}
                    onChangeText = {(text)=>{this.onLastNameChanged(text)}}
                    editable={this.state.edit ? true : false}
                        style={styles.textInput}
                        placeholder={'Last Name'}
                    />
                </TextInputLayout>
            </View>
            <View>
                  <TextInputLayout
                  value={this.props.email?this.props.email:null}
                  onChangeText = {(text)=>{this.onEmailChanged(text)}}
                    style={styles.inputLayout}
                    checkValid={t => EMAIL_REGEX.test(t)} focusColor="#d4d4d4">
                    <TextInput
                    editable={this.state.edit ? true : false}
                        style={styles.textInput}
                        placeholder={'Email'}
                    />
                </TextInputLayout>
            </View>
            <View style={{flexDirection:"row"}}>
            <TextInputLayout style={styles.inputLayout} focusColor="#d4d4d4">
                    <TextInput
                    value={this.props.phone}
                    onChangeText = {(text)=>{this.onNumberChanged(text)}}
                        editable={this.state.edit ? true : false}
                        style={styles.textInput}
                        placeholder={'Phone'}
                    />
                </TextInputLayout>
                <TextInputLayout style={styles.inputLayout} focusColor="#d4d4d4">
                    <TextInput
                    value={this.props.join_date}
                    onChangeText = {(text)=>{this.onJoinDateChanged(text)}}
                    editable={this.state.edit ? true : false}
                        style={styles.textInput}
                        placeholder={'Join Date'}
                    />
                </TextInputLayout>
            </View>
            <View>
                  <TextInputLayout
                  focusColor="#d4d4d4"
                    style={styles.inputLayout}>
                    <TextInput
                    value={this.props.address}
                    onChangeText = {(text)=>{this.onAddressChanged(text)}}
                    editable={this.state.edit ? true : false}
                        style={styles.textInput}
                        placeholder={'Address'}
                    />
                </TextInputLayout>
            </View>
            </View>
            {/* <View style={styles.viewStyle}> */}
            <TouchableOpacity style={[styles.button]} onPress={()=>{this.save()}}>
            <Text style={styles.buttonText}>SAVE</Text>
        </TouchableOpacity>
            {/* </View> */}
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    buttonText:{
        textAlign:"center",
        // justifyContent:"space-around",
        color:'white', 
        fontSize:15,
        margin: 0,
        textAlignVertical:"center",
        padding:0,
        fontFamily:'Comfortaa-Bold',
        fontWeight:"bold"
   },
    button: {
        
        // flex:1,
        height:60,
        // alignItems:"center",
        backgroundColor: '#378bde',
        // marginTop:10,
        marginLeft:20,
        marginRight:20,
        maxHeight:50,
        marginBottom:'20%',
        // width:"80%",
        // justifyContent: 'center',
      },
    imageStyle:{
        flex:2,
        margin:25,
        width: 100,
        height: 100,
        borderRadius: 75
    },
    container: {
        flex: 1,
        paddingTop: 100
    },
    textInput: {
        fontSize: 16,
        height: 40,
        width:150
    },
    inputLayout: {
        
        marginTop: 16,
        marginHorizontal: 20
    }
})
const mapStateToProps = ({auth}) =>{
    const {loading,error,fail,success,firstName,lastName,email,phone,address,join_date,isModalVisible} = auth;
    return{
        loading,
        error,
        firstName,
        lastName,
        phone,
        address,
        join_date,
        email,
        fail,
        success,
        isModalVisible
    };
};

const mapDispatchToProps = {
    editUser,
    fisrtNameChanged,
    lastNameChanged,
    emailChanged,
    addressChanged,
    phoneChanged,
    joinDateChanged,
    showAuthModal,
    hideAuthModal
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);

