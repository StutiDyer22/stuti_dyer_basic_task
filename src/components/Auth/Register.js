import React, { Component } from 'react'
import { Text, StyleSheet, View,TextInput,StatusBar,Image,TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import PhotoUpload from 'react-native-photo-upload'
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {registerUser,
    nameChanged,
    emailChanged,
    passwordChanged,
    showAuthModal,
    hideAuthModal} from "../../Actions"


class Register extends Component {
    constructor(props){
        super(props)
        this.state = {
            nameErr:false,
            emailErr:false,
            passErr:false,
            confirmPassErr:false,
            confirmPassword:''
        }
        
    }
    renderError(){
        if(this.props.fail){
            return(
                <View>
                    <Modal isVisible = {this.props.isModalVisible} 
                    style={styles.modalStyle}>
                    <View style={[styles.cardStyle, {height:'30%'}]}>
                    <Image source={require('../../assets/images/drawer.png')} resizeMode='contain' style={{
                    height:"25%", alignSelf:'center'}}/>
                    <Text style={{textAlign:'center', fontFamily:'Comfortaa-Bold', color:'rgb(51,51,51)', margin:'5%'}}>Invalid Credentials!</Text>
                    <TouchableOpacity style={[styles.buttonModal, {height:'30%'}]} onPress={() => this.toggleModal()}>
                        <Text style={{color:'white', textAlign:'center', fontSize:15, fontFamily:'Comfortaa-Bold'}}>Retry</Text>
                    </TouchableOpacity>
                    </View>
                    </Modal>
                </View>
            );
        }
        return null;
    }
    ValidateEmail = mail => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
          return true;
        }
        return false;
      };
    submit = ()=> {
        // console.log("props",this.props)
        const { username, email, password ,navigation} = this.props;
        if (username.length> 1) {
          this.setState({ nameErr: false });
          if (this.ValidateEmail(email)) {
            this.setState({ emailErr: false });
                 if (password.length > 5) {
                  this.setState({ passErr: false });
                  if( password === this.state.confirmPassword){
                      this.setState({confirmPassErr:false})
                    // this.props.navigation.navigate('Home');
                    this.props.registerUser(username,email,password, navigation);
                } else {
                    this.setState({confirmPassErr:true})
                }
              }  else {
                this.setState({ passErr: true });
            }
          } else {
            this.setState({ emailErr: true });
          }
        } else {
          this.setState({ nameErr: true });
        }
      };

    UNSAFE_componentWillUnmount(){
        this.props.nameChanged('')
        this.props.passwordChanged('');
        this.props.emailChanged('');
    }
    onUsernameChanged(text){
        this.props.nameChanged(text);
    }
    onEmailChanged(text){
        this.props.emailChanged(text);
    }
    onPasswordChanged(text){
        this.props.passwordChanged(text);
    }
    toggleModal = () => {
        // this.setState({ isModalVisible: !this.state.isModalVisible, name: '', points: '', product: '' });
        this.props.hideAuthModal();
      };
    render() {
  return (
        <View style={styles.container}>
        <StatusBar backgroundColor="#3CB371"/>
        <View style={{flex:1.5}}>
        {this.renderError()}
        <PhotoUpload
            onPhotoSelect={avatar => {
            if (avatar) {
            console.log('Image base64 string: ', avatar)
            }
        }}>
        <Image
        style={styles.imageStyle}
        resizeMode='cover'
        source={require('../../assets/images/profile.png')}
    />
    </PhotoUpload>
    </View>
        <View style={styles.viewStyle}>
        <Text style={styles.textStyle}>Username</Text>        
        <TextInput
        style={styles.textBox} 
        error={this.state.nameErr}
        value = {this.props.username}       
        onChangeText = {(text)=>{this.onUsernameChanged(text)}}
        placeholder = "Enter Username"
        placeholderTextColor = "#E5E5E5"
      />
      <Text style={styles.textStyle}>Email</Text>        
       <TextInput
        style={styles.textBox}   
        error={this.state.emailErr}   
        value={this.props.email}  
        onChangeText = {(text)=>{this.onEmailChanged(text)}}
        placeholder = "Enter Email"
        placeholderTextColor = "#E5E5E5"

      />
      </View>
      <View style={styles.viewStyle}>
    <Text style={styles.textStyle}>Password</Text>        
       <TextInput
        style={styles.textBox}        
        error = {this.state.passErr}
        value={this.props.password}
        onChangeText = {(text)=>{this.onPasswordChanged(text)}}
        placeholder = "Enter Password"
        secureTextEntry
        placeholderTextColor = "#E5E5E5"
      />
      <Text style={styles.textStyle}>Confirm Password</Text>        
       <TextInput
        style={styles.textBox}        
        error = {this.state.confirmPassErr}
        value={this.state.confirmPassword}
        onChangeText={(text)=>{this.setState({confirmPassword:text})}}
        placeholder = "Re-Enter Password"
        secureTextEntry
        placeholderTextColor = "#E5E5E5"
      />
      </View>
      
         <TouchableOpacity style={[styles.button]} onPress={()=>{this.submit()}}>
            <Text style={styles.buttonText}>GET STARTED</Text>
        </TouchableOpacity>
        
        
            <View style={{flexDirection:"row",flex:1,justifyContent:"center",alignItems:"center"}}>
                <Text style={styles.firstText}>
                    Already joined? </Text>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Login')}}>
                    <Text style={styles.seconText}>
                    Login</Text>
                </TouchableOpacity>
            </View>
        </View>
            
        )
    }
}

const styles = StyleSheet.create({
    buttonText:{
     color:'#3CB371', 
     textAlign:'center', 
     fontSize:15,
     fontFamily:'Comfortaa-Bold',
     fontWeight:"bold"
},
    firstText:{
        color:'white',
        fontSize:15,
        fontFamily:'Comfortaa-Bold'
    },
    seconText:{
        color:'white', 
        fontSize:15,
        fontFamily:'Comfortaa-Bold',
        fontWeight:"bold"
    },
    imageStyle:{
        padding:0,
        margin:0,
        // paddingVertical: 30,
        width: 100,
        height: 100,
        borderRadius: 75
    },
    container:{    
        backgroundColor:'#3CB371',
        height:"100%",
        flex:1
      
    },
    viewStyle:{
        flex:1.5,
        // marginBottom:30,
          alignItems:"center",
        justifyContent:"center"
    },
    textBox:{
        padding:10,
        height: 45, 
        width:"80%",
        borderRadius:10,
        borderColor:"grey",
        borderWidth:0.5,
        marginLeft:10,
        marginRight:10,
        marginTop:5,
        marginBottom:5,
        backgroundColor:"#2E8B57"
    },
    textStyle:{
        alignSelf:"flex-start",
        marginLeft:"10%",
        color:"white",
        fontSize:15
    },
      button: {
        //   flex:1,
        marginTop:10,
        // marginBottom:90,
        alignSelf:"center",
        height: '6%',
        textAlign:"center",
        width:"80%",
        borderRadius: 10,
        justifyContent: 'center',
        backgroundColor: 'white'
      },
      
})
const mapStateToProps = ({auth}) =>{
    const {loading,error,fail,success,username,email,password,isModalVisible} = auth;
    return{
        loading,
        error,
        username,
        email,
        password,
        fail,
        success,
        isModalVisible
    };
};

const mapDispatchToProps = {
    registerUser,
    nameChanged,
    emailChanged,
    passwordChanged,
    showAuthModal,
    hideAuthModal
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);