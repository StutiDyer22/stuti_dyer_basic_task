import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TextInput,
    StatusBar,
    Image,
    TouchableOpacity,
    Alert } from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {
    loginUser,
    nameChanged,
    passwordChanged,
    showAuthModal,
    hideAuthModal
} from "../../Actions"

class Login extends Component {
    constructor(props){
        super(props)
        this.state = {
            usernameErr:false,
            passErr:false,
            username:"",
            password:"",
        }
    }
    UNSAFE_componentWillUnmount(){
        this.props.nameChanged('')
        this.props.passwordChanged('');
    }
    onUsernameChanged(text){
        this.props.nameChanged(text);
    }
    onPasswordChanged(text){
        this.props.passwordChanged(text);
    }
    toggleModal = () => {
        // this.setState({ isModalVisible: !this.state.isModalVisible, name: '', points: '', product: '' });
        this.props.hideAuthModal();
      };


login = () => {
    console.log("user",this.state.usernameErr,this.state.passErr)

    const { username, password, navigation } = this.props;
    if (username.length>1) {
      this.setState({ usernameErr: false });
      if (password.length > 5) {
        this.setState({ passErr: false });
        this.props.loginUser(username, password, navigation);
      } else {
        this.setState({ passErr: true });
      }
    } else {
        
      this.setState({ 
          usernameErr: true,
        });
      console.log("user",this.state.usernameErr)
    }
    // if (this.props.email && this.props.password) {
    //   this.setState({ error: false });
    //   this.props.loginUser(
    //     this.props.email,
    //     this.props.password,
    //     this.props.navigation
    //   );
    // } else {
    //   this.setState({ error: true });
    // }
  };

    renderError(){
        if(this.props.fail){
            return(
                <View>
                    <Modal isVisible = {this.props.isModalVisible} 
                    style={styles.modalStyle}>
                    <View style={[styles.cardStyle, {height:'30%'}]}>
                    <Image source={require('../../assets/images/drawer.png')} resizeMode='contain' style={{
                    height:"25%", alignSelf:'center'}}/>
                    <Text style={{textAlign:'center', fontFamily:'Comfortaa-Bold', color:'rgb(51,51,51)', margin:'5%'}}>Invalid Credentials!</Text>
                    <TouchableOpacity style={[styles.buttonModal, {height:'30%'}]} onPress={() => this.toggleModal()}>
                        <Text style={{color:'white', textAlign:'center', fontSize:15, fontFamily:'Comfortaa-Bold'}}>Retry</Text>
                    </TouchableOpacity>
                    </View>
                    </Modal>
                </View>
            );
        }
        return null;
    }
      
    render() {
        
        return (
        <View style={styles.container}>
        <StatusBar backgroundColor="#3CB371"/>
        {this.renderError()}
        <Image
              source={require('../../assets/images/logo.png')}
              style={styles.logo}
              resizeMode="contain"
            />
        <Text style={styles.textStyle}>Username</Text>        
        <TextInput
        label={
            this.state.usernameErr
            ?'PLEASE ENTER VALID USERNAME'
            :'Username'
        }
        value={this.props.username}
        onChangeText={(text)=>{this.onUsernameChanged(text)}}
        error={this.state.usernameErr}
        style={styles.textBox}        
        placeholder = "Enter Username"
        placeholderTextColor = "#E5E5E5"
        />
    <Text style={styles.textStyle}>Password</Text>        
       <TextInput
       label={
        this.state.usernameErr
            ?'PLEASE ENTER VALID PASSWORD'
            :'Password'
        }
        value={this.props.password}
        onChangeText={this.onPasswordChanged.bind(this)}
        error={this.state.passErr}
        style={styles.textBox}     
        secureTextEntry   
        placeholder = "Enter Password"
        placeholderTextColor = "#E5E5E5"

      />
      <View style={{paddingLeft:"45%",paddingTop:"5%"}}>
       <TouchableOpacity>
            <Text style={styles.forgotPasswordbutton}>
            Forgot Password?</Text>
        </TouchableOpacity>
        </View>
         <TouchableOpacity style={[styles.button]} onPress={()=>this.login()}>
            <Text style={{color:'#3CB371', textAlign:'center', fontSize:15, fontFamily:'Comfortaa-Bold',fontWeight:"bold"}}>SIGN IN</Text>
        </TouchableOpacity>
        <View style={{bottom:1}}>
        <View style={{flexDirection:"row",alignSelf:"flex-end"}}>
        <Text style={{color:'white', fontSize:15, fontFamily:'Comfortaa-Bold'}}>
            Haven't joined yet?</Text>
       <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Register')}}>
            <Text style={{color:'white', fontSize:15, fontFamily:'Comfortaa-Bold',fontWeight:"bold"}}>
            Join Now</Text>
        </TouchableOpacity>
        </View>
        </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#3CB371',
        height:"100%",
        alignItems:"center",
        justifyContent:"center"
    },
    cardStyle:{
        height:'85%',
        width:'90%',
        padding:'5%',
        paddingBottom:0,
        elevation:6,
        backgroundColor:'white',
        borderRadius:10,
      },
    textBox:{
        height: 45, 
        width:"80%",
        borderRadius:10,
        borderColor:"grey",
        borderWidth:0.5,
        marginLeft:10,
        marginRight:10,
        marginTop:5,
        marginBottom:5,
        backgroundColor:"#2E8B57"
    },
    textStyle:{
        alignSelf:"flex-start",
        marginLeft:"10%",
        color:"white",
        fontSize:15
    },
    buttonModal: {
        height: '10%',
        width: '100%',
        borderRadius: 8,
        justifyContent: 'center',
        backgroundColor: 'rgb(41,34,108)'
      },
    logo: {
        marginBottom:20,
        // marginTop:-50,
        height: '20%',
        width: '80%',
      },
      modalStyle:{
        justifyContent:'center',
        alignItems:'center'
      },
      button: {
        marginBottom:50,
        marginTop:'1%',
        height: '6%',
        width:"80%",
        borderRadius: 10,
        justifyContent: 'center',
        backgroundColor: 'white'
      },
      forgotPasswordbutton:{
      color:'white', 
      fontSize:15, 
      fontFamily:'Comfortaa-Bold',
      fontWeight:"bold"
    }
})
const mapStateToProps = ({auth}) =>{
    const {loading,error,fail,success,username,password,isModalVisible} = auth;
    return{
        loading,
        error,
        username,
        password,
        fail,
        success,
        isModalVisible
    };
};

const mapDispatchToProps = {
    loginUser,
    nameChanged,
    passwordChanged,
    showAuthModal,
    hideAuthModal
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
