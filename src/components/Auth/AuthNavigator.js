import React from 'react';
import Login from "../Auth/Login";
import Register from "../Auth/Register"
import {
    createStackNavigator,
    createAppContainer,
  } from 'react-navigation';
const AuthNavigator = createStackNavigator({
    Login: Login,
    Register: Register,
  },
  {
    headerMode: "none"
  })

const AuthContainer = createAppContainer(AuthNavigator);
export default AuthContainer;