import React from 'react';
import { View, Text, TouchableWithoutFeedback, StyleSheet,Image } from 'react-native';
import {Icon} from 'react-native-elements';
import {connect} from "react-redux"
 class CustomDrawer extends React.Component {
    render(){
        return(
            <View style={{flex:1, margin:'5%'}}>
            <View style={{justifyContent:"center",alignItems:"center",paddingTop:"20%"}}>
            <TouchableWithoutFeedback onPress = {()=>{console.log("props",this.props.username)}}>
          <Image
            source={require('../../assets/images/drawer.png')}
            style={{ width: 90, height: 90}}
            
          />
          </TouchableWithoutFeedback>
          <Text style={{color:"white"}}>{this.props.user}</Text>
          </View> 
                
                <View style={{flex:9}}>
               <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Home')} style={{alignItems:'center'}}>
                   <View style={styles.titleStyle}>
                        
                        <Text style={{color:'white', marginLeft:'2%', fontSize:18,alignSelf:"center"}}>Home</Text>
                   </View>
               </TouchableWithoutFeedback>
               <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TestStrip')} style={{alignItems:'center', justifyContent:'center'}}>
                   <View style={styles.titleStyle}>
                        
                        <Text style={{color:'white', marginLeft:'2%', fontSize:18,alignSelf:"center"}}>Test Strip</Text>
                   </View>
               </TouchableWithoutFeedback>
               <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Profile')} style={{alignItems:'center', justifyContent:'center'}}>
                   <View style={styles.titleStyle}>
                        
                        <Text style={{color:'white', marginLeft:'2%', fontSize:18,alignSelf:"center"}}>Profile</Text>
                   </View>
               </TouchableWithoutFeedback>
               <TouchableWithoutFeedback onPress={() => {
                //    AsyncStorage.removeItem('Login')
                   this.props.navigation.navigate('Auth')
                   }} style={{alignItems:'center', justifyContent:'center'}}>
                   <View style={styles.titleStyle}>
                        
                        <Text style={{color:'white', marginLeft:'2%', fontSize:18,alignSelf:"center"}}>Sign Out</Text>
                   </View>
               </TouchableWithoutFeedback>
               </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    titleStyle:{

        flex:0.08,
        height:'6%',
        marginTop:'5%',
        // alignItems:'center',
        // justifyContent:"center",
        // borderBottomWidth:0.3,
        flexDirection:'row'
    },
    logoText:{

        fontSize:25,
        color:'black',
      },
})


const mapStateToProps = ({auth}) => {
    console.log("auth",auth.user)
    const{user} = auth;
    return{
        user
    };
}


export default connect(
    mapStateToProps,
    null
)(CustomDrawer);