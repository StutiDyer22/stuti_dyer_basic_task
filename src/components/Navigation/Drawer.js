import React, { Component } from 'react';
import { View, Image, TouchableOpacity,Text } from 'react-native';
import AuthContainer from "../Auth/AuthNavigator"
import {
  createDrawerNavigator,
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  DrawerItems
} from 'react-navigation';
import Home from "../MainScreens/Home";
import TestStrip from "../MainScreens/TestStrip";
import Profile from "../MainScreens/Profile";

import CategoryDetails from "../MainScreens/CategoryDetail";
import CustomDrawer from "./CustomDrawer"



const MainNavigator = createStackNavigator({
  Home:Home,
  TestStrip:TestStrip,
  Profile:Profile,
  CategoryDetails:CategoryDetails
},{
  headerMode:"none"
})
const MainContainer = createAppContainer(MainNavigator)

const Drawer = createDrawerNavigator(
  {
    MainContainer
},{
  contentComponent: CustomDrawer,
  drawerBackgroundColor:"#6492f5",
  
    // initialRouteName:"Home",
  // activeTintColor: 'white',
  activeBackgroundColor: '#6492f5',
  inactiveTintColor: 'white',
  labelStyle: 'normal'
  
  }
);
const DrawerNavigator = createAppContainer(Drawer);

const switchNavigator = createSwitchNavigator({
  Auth: AuthContainer,
  Main: DrawerNavigator
});
const MaNavigator = createAppContainer(switchNavigator);
 
export default MaNavigator;